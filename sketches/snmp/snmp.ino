/*
 This sketch enables/disables interfaces set in ports[] array in a remote device
 */

#include <avr/pgmspace.h> //Library needed to store data into flash memory

#include <MemoryFree.h>
#include <SPI.h>         //  needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h> //  UDP library from: bjoern@cs.stanford.edu 12/30/2008

#include "MyTypes.h"


/*                                    //
              UDP Data                //
*/                                    //
//IPAddress local(192, 168, 2, 4);
//IPAddress remote(192, 168, 2, 2);
IPAddress local(192, 168, 1, 51);
IPAddress remote(192, 168, 1, 6);

UDP_struct udp = {
  {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  }
  , local, remote, 62254
};

/*                                    //
              Global Data             //
*/                                    //
byte packetBuffer[UDP_TX_PACKET_MAX_SIZE];
unsigned long requestID = 32760;
byte selectedInterface, endMode = 0;

//  UDP instance
EthernetUDP Udp;

/*                                    //
         STATIC DATA -> Flash         //
*/                                    //
//---------INTERFACES----------0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 34 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40
const byte ports[] PROGMEM  = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
const char community[] PROGMEM = {"wr1te"};
const int interfacesOID[] PROGMEM = {1, 3, 6, 1, 2, 1, 2, 2, 1, 7, 1};
const int SysDescrOID[] PROGMEM = {1, 3, 6, 1, 2, 1, 1, 200, 0};


void setup() {
  Serial.begin(9600);
  Serial.print(F("Free Ram: "));
  Serial.println(freeMemory());
  Serial.println(F("Trying DHCP..."));

  // Start Ethernet and UDP
  if (Ethernet.begin(udp.mac) == 0) {
    Serial.println(F("Failed to configure Ethernet using DHCP"));
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(udp.mac, udp.local_IP);
    Serial.print(F("Static ip: "));
    Serial.println(udp.local_IP);
  }
  else {
    Serial.print(F("DHCP IP: "));
    for (byte i = 0; i < 4; i++) {
      // print the value of each byte of the IP address:
      Serial.print(Ethernet.localIP()[i], DEC);
      Serial.print(F("."));
    }
    Serial.println();
  }
  // give the Ethernet shield a second to initialize:
  delay(1000);
  Udp.begin(udp.localPort);

  selectedInterface = 5;
}

void loop() {
  // mode 1 -> set 1 in all interfaces
  mode(1);
  delay(5000);
  // mode 5 -> check interfaces status
  mode(5);
  delay(5000);
  // do nothing forever
  Serial.print(F("going to sleep..."));
  while (true);
}

/**
 * mode function: selects mode.
 **/
byte mode(byte mode) {
  switch (mode) {
    case 1: case 2: case 5:
      {
        response_struct result;
        //load OID
        int OID[sizeof(interfacesOID) / sizeof(typeof(interfacesOID[0]))];
        for (byte i = 0; i < sizeof(OID) / sizeof(typeof(OID[0])); i++) {
          OID[i] = pgm_read_word_near(interfacesOID + i);
        }
        for (byte i = 1; i < sizeof(ports) && i <= INTERFACES; i++) {
          if (mode == 1) {
            if (i != SELFINTERFACE && pgm_read_byte_near(ports + i) == 1) {
              //set OID to desired interface
              OID[sizeof(OID) / sizeof(typeof(OID[0])) - 1] = i;
              result = sendSNMPMessage(0xA3, OID, sizeof(OID) / sizeof(typeof(OID[0])), 0x02, 1, F(""));
            }
            else result.error = -1;
          }
          else if (mode == 2) {
            if (i != SELFINTERFACE && pgm_read_byte_near(ports + i) == 1) {
              //set OID to desired interface
              OID[sizeof(OID) / sizeof(typeof(OID[0])) - 1] = i;
              result = sendSNMPMessage(0xA3, OID, sizeof(OID) / sizeof(typeof(OID[0])), 0x02, 1, F(""));
            }
            else result.error = -1;
          }
          else if (mode == 5) {
            OID[sizeof(OID) / sizeof(typeof(OID[0])) - 1] = i;
            result = sendSNMPMessage(0xA0, OID, sizeof(OID) / sizeof(typeof(OID[0])), 0x00, 0, F(""));
          }

          if (result.error == -3) {
            delay(2000);
            endMode = 1;
          }
          delay(500); // PROBLEMA DESPUES DE NO PERMITIDO. RESULTADO MUY RÁPIDO
          if (endMode == 1) {
            endMode = 0;
            break;
          }
        }
        break;
      }
    case 3: case 4: case 6:
      {
        response_struct result;
        //load OID
        int OID[sizeof(interfacesOID) / sizeof(typeof(interfacesOID[0]))];
        for (byte i = 0; i < (sizeof(OID) / sizeof(typeof(OID[0]))); i++) {
          OID[i] = pgm_read_word_near(interfacesOID + i);
        }
        OID[sizeof(OID) / sizeof(typeof(OID[0])) - 1] = selectedInterface;
        if (selectedInterface <= INTERFACES && selectedInterface != SELFINTERFACE && pgm_read_byte_near(ports + selectedInterface) == 1) {
          if (mode == 3) {
            result = sendSNMPMessage(0xA3, OID, (sizeof(OID) / sizeof(typeof(OID[0]))), 0x02, 1, F(""));
          }
          else if (mode == 4) {
            result = sendSNMPMessage(0xA3, OID, sizeof(OID) / sizeof(typeof(OID[0])), 0x02, 2, F(""));
          }
          else if (mode == 6) {
            result = sendSNMPMessage(0xA0, OID, sizeof(OID) / sizeof(typeof(OID[0])), 0x00, 0, F(""));
          }
        }
        else {
          result.error = -1;
        }
        delay(5000);
        break;
      }
    case 7:
      {
        response_struct result;
        int OID[sizeof(SysDescrOID) / sizeof(typeof(SysDescrOID[0]))];
        for (byte i = 0; i < sizeof(OID) / sizeof(typeof(OID[0])); i++) {
          OID[i] = pgm_read_word_near(SysDescrOID + i);
        }
        result = sendSNMPMessage(0xA3, OID, sizeof(OID) / sizeof(typeof(OID[0])), 0x04, 0, F("Hardware: AMD64 Family 20 Model 2 Stepping 0 AT/AT COMPATIBLE - Software: Windows Version 6.3 (Build 9600 Multiprocessor Free)"));
        delay(5000);
        break;
      }
  }
}

/**
  sendSNMPMessage function: sends SNMP message
  error codes:
    -1 is reserved
    -2 -> error in SNMP response
    -3 -> no response
    -4 -> too large message
    -5 -> too large response
 **/
response_struct sendSNMPMessage(byte pduType, int OID[], byte OIDSize, byte valueType, unsigned long valueInt, const __FlashStringHelper * text) {
  clearUDPBuffer();
  //GetRequest = 0xA0. GetNextRequest = 0xA1 SetRequest = 0xA3
  int packetSize;
  boolean sent;
  byte timeout;
  byte retry = 0;
  byte textSize = 0;
  byte communitySize = 0;
  SNMP_struct receivedPacket;
  response_struct response;

  //find text length
  char *ptr = ( char * ) text;
  while (pgm_read_byte_near( ptr + textSize ) != '\0') {
    textSize++;
  }

  //find community length
  while (pgm_read_byte_near( community + communitySize ) != '\0') {
    communitySize++;
  }

  //check max buffer sizes
  if (textSize > MAX_VALUE_SIZE || communitySize > MAX_COMMUNITY_SIZE) {
    response.error = -4;
    return response;
  }
  while (retry < 5) { //we'll break this loop with a return.
    packetSize = 0;
    timeout = 0;
    sent = 0;
    memset(&packetBuffer[0], 0, sizeof(packetBuffer));
    memset(&receivedPacket, 0, sizeof(receivedPacket));

    int generatedBytes = generatePacket(OID, OIDSize, valueType, valueInt, text, textSize, pduType);
    if (generatedBytes == -1) {
      response.error = -4;
      return response;
    }

    //reverse array. Remember we generate bytes from right to left
    reverseArray(0, generatedBytes - 1);


    parsePacket(generatedBytes, &receivedPacket);
    printPacket(generatedBytes, &receivedPacket);
    Serial.print(F("\n\n"));
    requestID++;

    Serial.println(F("----------------- Sending packet -----------------"));
    writePacket(generatedBytes);
    sent = 1;


    //... receive packet

    packetSize = Udp.parsePacket();
    while (packetSize == 0 && sent) { //wait until GetResponse arrives, in case we sent a request for this port
      packetSize = Udp.parsePacket();
      Serial.println(F("waiting..."));
      delay(500);
      if (++timeout > 10) { // if we don't receive a response in 5 seconds, timeout
        retry++;
        Serial.print(F("-- retrying: "));
        Serial.println(retry);
        break;
      }
    }
    if (packetSize) {
      retry = 0;

      //clear buffers
      memset(&packetBuffer[0], 0, sizeof(packetBuffer));
      memset(&receivedPacket, 0, sizeof(receivedPacket));

      if (packetSize < UDP_TX_PACKET_MAX_SIZE) {
        receivePacket(packetSize);
        //SNMP_struct receivedPacket;
        parsePacket(packetSize, &receivedPacket);
        printPacket(packetSize, &receivedPacket);
        Serial.print(F("Free Ram: "));
        Serial.println(freeMemory());
        if (receivedPacket.requestID != requestID - 1) {
          response.error = -2;
          return response;
        }
        else if (receivedPacket.error != 0x00) {
          response.error = receivedPacket.error;
          return response;
        }
        else { //no error
          response.error = 0;
          response.value = receivedPacket.valueInt;
          return response;
        }
      }
      else {
        Serial.println(F("Packet too large"));
        delay(1000);
        //clear trash in buffer, just in case
        clearUDPBuffer();
        response.error = -5;
        return response;
      }
    }
  }
  //clear trash in buffer, just in case
  clearUDPBuffer();
  Serial.println(F("---- NO RESPONSE"));
  response.error = -3; //no response
  return response;
}

void clearUDPBuffer() {
  //clear trash in buffer, just in case
  int packetSize = Udp.parsePacket();
  while (packetSize) {
    delay(1000);
    Udp.read(packetBuffer, 0);
    packetSize = Udp.parsePacket();
    memset(&packetBuffer[0], 0, sizeof(packetBuffer));
  }
}


/**
 * receivePacket function: reads UDP packet and stores it into packetBuffer
 **/
int receivePacket(int packetSize) {
  Serial.print(F("UDP packet received. Size (bytes): "));
  Serial.println(packetSize);
  Serial.print(F("From "));
  IPAddress remote = Udp.remoteIP();
  for (byte i = 0; i < 4; i++)
  {
    Serial.print(remote[i], DEC);
    if (i < 3)
    {
      Serial.print(F("."));
    }
  }
  Serial.print(F(", port "));
  Serial.println(Udp.remotePort());

  //  read packetSize bytes and store them into packetBuffer
  Udp.read(packetBuffer, packetSize);
}

/**
 * writePacket function: writes bytes into packetBuffer and sends packet over UDP
 **/
int writePacket(int generatedBytes) {
  Udp.beginPacket(udp.remote_IP, 161);
  for (int i = 0; i < generatedBytes; i++) {
    Udp.write(packetBuffer[i]);
  }
  Udp.endPacket();
  return 1;
}

/**
 * printPacket function: reads data from a struct and prints it on Serial Monitor in a human readable format
 **/
void printPacket (int packetSize, SNMP_struct * receivedPacket) {
  Serial.print(F("  Details: "));
  Serial.print(F("size: "));
  Serial.println(receivedPacket->snmp_length);
  Serial.print(F("  Version: "));
  Serial.print(receivedPacket->snmpVersion);
  Serial.print(F(". Community: "));
  Serial.println(receivedPacket->community);
  Serial.print(F("  Request ID: "));
  Serial.print(receivedPacket->requestID);

  if (receivedPacket->error == 0x01) {
    Serial.print(F(". Error: Response message too large to transport"));
  }
  else if (receivedPacket->error == 0x02) {
    Serial.print(F(". Error: No Such Name"));
  }
  else if (receivedPacket->error == 0x03) {
    Serial.print(F(". Error: Data type error"));
  }
  else if (receivedPacket->error == 0x04) {
    Serial.print(F(". Error: Read-Only value"));
  }
  else if (receivedPacket->error == 0x05) {
    Serial.print(F(". Error: General error"));
  }

  if (receivedPacket->pduType == 0xA0)
    Serial.println(F(". PDU type: GetRequest"));
  else if (receivedPacket->pduType == 0xA2)
    Serial.println(F(". PDU type: GetResponse"));
  else if (receivedPacket->pduType == 0xA3)
    Serial.println(F(". PDU type: SetRequest"));

  Serial.print(F("  OID: "));
  Serial.print("1.3.");
  for (int i = 1; i < receivedPacket->OIDLength; i++) {
    Serial.print(receivedPacket->OID[i], DEC);
    Serial.print(F("."));
  }

  if (receivedPacket->valueType == 0x02 || receivedPacket->valueType == 0x41 || receivedPacket->valueType == 0x42 || receivedPacket->valueType == 0x43) {
    Serial.print(F(" Value: "));
    Serial.println(receivedPacket->valueInt);
  }
  else if (receivedPacket->valueType == 0x04) {
    Serial.print(F(" Value: "));
    Serial.println(receivedPacket->valueChar);
  }
  else if (receivedPacket->valueType == 0x05) {
    Serial.print(F(" Value: "));
    Serial.println(F("NULL"));
  }

  // debugging
  if (DEBUG) {
    Serial.println(F("Content:"));
    for (int x = 0; x < packetSize; x++) {
      if (x % 16 == 0 && x != 0)
        Serial.println();
      Serial.print(packetBuffer[x], HEX);
      Serial.print(F(", "));
    }
    Serial.print(F("\n\n"));
  }
}

/**
 * generatePacket function: writes plain data into packetbuffer as bytes so it can be sent over UDP
 **/
int generatePacket(int OID[], byte OIDSize, byte valueType, unsigned long valueInt, const __FlashStringHelper * text, int valueCharSize, byte pduType) {
  int  snmpCursor = 0;
  //write value. It can be null, Integer or OctetString
  byte valueLength = countBytes(valueInt);
  if (pduType == 0xA0 || valueType == 0x05) {
    packetBuffer[snmpCursor++] = 0x00;
    packetBuffer[snmpCursor++] = 0x05;
  }
  else if (valueType == 0x02 && valueLength == 1) { // one byte integer
    packetBuffer[snmpCursor++] = valueInt;
    packetBuffer[snmpCursor++] = 0x01;
    packetBuffer[snmpCursor++] = 0x02;
  }
  else if (valueType == 0x02 && valueLength > 1) { // 2 or more bytes integer
    byte buffer[valueLength];
    writeLongValue(&buffer[0], valueInt);
    for (int i = 0; i < valueLength ; i++) {
      packetBuffer[snmpCursor++] = buffer[i];
    }
    packetBuffer[snmpCursor++] = valueLength;
    packetBuffer[snmpCursor++] = 0x02;
  }
  else if (valueType == 0x04) { // OctecString
    char *ptr = ( char * ) text;
    for (int i = valueCharSize - 1; i >= 0; i--) {
      packetBuffer[snmpCursor++] = (char)pgm_read_byte_near( ptr + i );
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
    }
    if (countBytes(valueCharSize) == 1) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor++] = valueCharSize;
    }
    else {
      byte buffer[countBytes(valueCharSize) + 1];
      writeVLQ(&buffer[0], valueCharSize);
      for (byte i = 0; i < countBytes(valueCharSize) + 1; i++) {
        if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
          return -1;
        packetBuffer[snmpCursor++] = buffer[i];
      }
    }
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor++] = 0x04;
  }
  // write OID
  int realOIDSize = 0;
  for (int i = OIDSize - 1; i > 1; i--) { // i > 1 because we're not writing 1.3 component here, it'll be coded as a single byte
    if (countBytes(OID[i]) == 1) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor] = OID[i];
      snmpCursor++;
      realOIDSize++;
    }
    else {
      byte buff[countOIDBytes(OID[i])];
      writeOIDComponent(&buff[0], OID[i]);
      for (int j = 0; j < countOIDBytes(OID[i]); j++) {
        if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
          return -1;
        packetBuffer[snmpCursor++] = buff[j];
        realOIDSize++;
      }
    }
  }
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = 0x2B; //0x2B == 1.3
  realOIDSize++;
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = realOIDSize;
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = 0x06; //OID Type == 0x06
  int tmp = countBytes(snmpCursor);
  if (tmp == 1) {
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
    snmpCursor++;
  }
  else {
    byte buffer[tmp + 1];
    writeVLQ(&buffer[0], snmpCursor);
    for (byte i = 0; i < tmp + 1; i++) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor++] = buffer[i];
    }
  }
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = 0x30;
  tmp = countBytes(snmpCursor);
  if (tmp == 1) {
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
    snmpCursor++;
  }
  else {
    byte buffer[tmp + 1];
    writeVLQ(&buffer[0], snmpCursor);
    for (byte i = 0; i < tmp + 1; i++) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor++] = buffer[i];
    }
  }
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = 0x30;

  // write (no) error codes
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 3; j++) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor++] = j;
    }
  }
  //write request id
  if (countBytes(requestID) == 1) {
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor++] = requestID; // Global variable, assume 1 byte
  }
  else { //requestID is larger than 1 byte. It needs to be processed by writeLongValue function first
    byte buffer[countBytes(requestID)];
    writeLongValue(&buffer[0], requestID);
    for (int i = 0; i < countBytes(requestID) ; i++) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor++] = buffer[i];
    }
  }
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = countBytes(requestID);
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = 0x02; // Integer type for requestID

  // write pdu type and size
  tmp = countBytes(snmpCursor);
  if (tmp == 1) {
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
    snmpCursor++;
  }
  else {
    byte buffer[tmp + 1];
    writeVLQ(&buffer[0], snmpCursor);
    for (byte i = 0; i < tmp + 1; i++) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor++] = buffer[i];
    }
  }
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = pduType;

  byte communitySize = 0;
  //find community length
  while (pgm_read_byte_near( community + communitySize ) != '\0') {
    communitySize++;
  }

  // write community
  for (int i = communitySize - 1; i >= 0; i--) {
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor++] = pgm_read_byte_near( community + i);
  }
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = communitySize;
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor++] = 0x04; //OctetString type for community

  for (int j = 0; j < 3; j++) { //Write version. 0 -> v1
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor++] = j;
  }
  //write packet size
  tmp = countBytes(snmpCursor);
  if (tmp == 1) {
    if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
      return -1;
    packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
    snmpCursor++;
  }
  else {
    byte buffer[tmp + 1];
    writeVLQ(&buffer[0], snmpCursor);
    for (byte i = 0; i < tmp + 1; i++) {
      if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
        return -1;
      packetBuffer[snmpCursor++] = buffer[i];
    }
  }
  if (snmpCursor > UDP_TX_PACKET_MAX_SIZE)
    return -1;
  packetBuffer[snmpCursor] = 0x30;

  return snmpCursor + 1;
}

int writeVLQ(byte * buffer, unsigned long n) {
  byte bytes = countBytes(n);
  buffer[bytes] = bytes;
  buffer[bytes] = buffer[bytes] | 0x80;
  for (byte i = 0; i < bytes; i++) {
    buffer[i] = n >> 8 * i;
  }
  return bytes + 1;
}

int writeOIDComponent(byte * buffer, int component) {
  byte bytes = countOIDBytes(component);
  for (byte i = 0; i < bytes; i++) {
    buffer[i] = component >> 7 * i;
    if (i == 0) {
      buffer[i] = buffer [i] & 0x7f;
    }
    else {
      buffer[i] = buffer[i] | 0x80;
    }
  }
  return bytes;
}

/**
 * writeLongValue function: Divides an unsigned long into bytes and writes it into *buffer
 **/
int writeLongValue(byte * buffer, unsigned long n) {
  byte bytes = countBytes(n);
  for (byte i = 0; i < bytes; i++) {
    buffer[i] = n >> 8 * i;
  }
  return bytes;
  //now in buffer we have n divided in bytes, ready to be sent
}

/**
 * countBytes function: Count bytes of an unsigned long in two compliment's form
 **/
byte countBytes(unsigned long n) {
  byte bytes;
  // get number size (bytes). As per ASN.1, most significant bit is not used for the value itself, so we have 7 bits for each number (128 numbers with 1 byte)
  if (n < 128) {
    bytes = 1;
  }
  else if (n < 32768) { // We have 7 + 8 bits = 15 bits. 2^15 = 32768. Two compliment's form.
    bytes = 2;
  }
  else if (n < 8388608) {
    bytes = 3;
  }
  else {
    bytes = 4;
  }
  return bytes;
}

/**
 * countBytes function: Count bytes of an unsigned long in two compliment's form
 **/
byte countOIDBytes(unsigned long n) {
  byte bytes;
  if (n < 128) {
    bytes = 1;
  }
  else if (n < 16384) { // We have 7 + 7 bits = 14 bits. 2^14 = 16384.
    bytes = 2;
  }
  else if (n < 2097152) {
    bytes = 3;
  }
  else {
    bytes = 4;
  }
  return bytes;
}


/**
 * parseVLQ function: parse variable length number for OIDs
 **/
int parseVLQ(int *snmpCursor) {
  int value;
  if (packetBuffer[*snmpCursor] & 0x80) { // if first bit is 1, then, the number is composed of 2 or more bytes. We asume we won't receive messages with length > 2 bytes (16384)
    int bytes = packetBuffer[*snmpCursor] & 0x7F; // set to 0 most significant bit and get the number of bytes
    (*snmpCursor)++;
    value = parseInt(&(*snmpCursor), bytes);
  }
  else {
    value = packetBuffer[*snmpCursor];
    (*snmpCursor)++;
  }
  return value;
}

/**
 * parsePacket function: This function reads a packet from packetBuffer, saves variables into a struct and returns number of read bytes
 **/
int parsePacket(int packetSize, SNMP_struct * receivedPacket) {
  int snmpCursor = 0;
  // check we received an SNMP message
  if (packetBuffer[snmpCursor] != 0x30) {
    Serial.println(F("not an SNMP message"));
    return 0;
  }
  else {
    Serial.println(F("  SNMP message. Parsing packet..."));
    snmpCursor++;
    receivedPacket->snmp_length = parseVLQ(&snmpCursor);

    // parse SNMP version
    if (packetBuffer[snmpCursor] == 0x02 && packetBuffer[++snmpCursor] == 0x01) { // correct version field
      if (packetBuffer[++snmpCursor] == 0x00)
        receivedPacket->snmpVersion = 1;
      else if (packetBuffer[snmpCursor] == 0x01)
        receivedPacket->snmpVersion = 2;
      else if (packetBuffer[snmpCursor] == 0x02)
        receivedPacket->snmpVersion = 3;
    }
    else {
      receivedPacket->snmpVersion = 0; //wrong version
      Serial.println(F("Wrong version"));
      return 0;
    }

    // parse community string
    if (packetBuffer[++snmpCursor] == 0x04) { // correct community string
      snmpCursor++; // let snmpCursor pointing to community string length
      receivedPacket->community_length = (int) packetBuffer[snmpCursor];
      parseOctecString(receivedPacket->community, &snmpCursor, MAX_COMMUNITY_SIZE);
    }
    else {
      return 0; //Wrong community string
    }

    // parse PDU type
    receivedPacket->pduType = packetBuffer[snmpCursor];
    snmpCursor++;

    receivedPacket->pdu_length = parseVLQ(&snmpCursor);

    // parse request id
    if (packetBuffer[snmpCursor] == 0x02) { // correct request id type (integer)
      byte valueLength = packetBuffer[++snmpCursor];
      snmpCursor++;
      receivedPacket->requestID = parseInt(&snmpCursor, valueLength);
    }

    // parse error
    if (packetBuffer[snmpCursor] == 0x02) { //  correct error type (integer)
      snmpCursor += 2;
      receivedPacket->error = packetBuffer[snmpCursor++];
    }

    // parse errorIndex
    if (packetBuffer[snmpCursor] == 0x02) { //  correct error type (integer)
      snmpCursor += 2;
      receivedPacket->errorIndex = packetBuffer[snmpCursor++];
    }

    // parse varBind
    if (packetBuffer[snmpCursor++] == 0x30) { // correct varbind type (sequence)
      //snmpCursor += 4; //let snmpCursor pointing to first OID byte. We ignore multiple OIDs at the moment.
      receivedPacket->varBindList_length = parseVLQ(&snmpCursor);
      if (packetBuffer[snmpCursor++] == 0x30) {
        receivedPacket->varBind_length = parseVLQ(&snmpCursor);
        receivedPacket->OIDLength = parseOID(&snmpCursor, receivedPacket->OID);
      }
    }

    // parse Integers, Counters, Gauges and Timeticks
    if (packetBuffer[snmpCursor] == 0x02 || packetBuffer[snmpCursor] == 0x41 || packetBuffer[snmpCursor] == 0x42 || packetBuffer[snmpCursor] == 0x43) {
      receivedPacket->valueType = packetBuffer[snmpCursor++];
      byte valueLength = packetBuffer[snmpCursor++];
      receivedPacket->valueInt = parseInt(&snmpCursor, valueLength);
    }

    //parse octec strings
    else if (packetBuffer[snmpCursor] == 0x04) {
      receivedPacket->valueType = packetBuffer[snmpCursor];
      snmpCursor++; //let snmpCursor pointing to value string length
      parseOctecString(receivedPacket->valueChar, &snmpCursor, MAX_VALUE_SIZE);
    }

    else if (packetBuffer[snmpCursor] == 0x05) {
      receivedPacket->valueType = packetBuffer[snmpCursor];
      snmpCursor++;
    }

    if (snmpCursor >= packetSize) {
      Serial.print(F("  Packet parsing ended successfully. snmpCursor: "));
      Serial.println(snmpCursor);
      Serial.println();
      return 1;
    }
    else {
      // continue parsing next OID. Not supported
    }
  }
}

/**
 * parseOID function: Parse OID and save it into OID[]. Returns OID length. It only supports components < 16384, although we can send higher components, they just won't
 * be parsed correctly on serial monitor. Parsing larger values would require to use long values instead of int, wasting a lot of memory in most cases
 **/
int parseOID(int *snmpCursor, int OID[]) {
  byte OIDLength;
  if (packetBuffer[*snmpCursor] == 0x06) { // correct OID type
    (*snmpCursor)++;
    OIDLength = (int) packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    for (byte i = 0; i < OIDLength; i++) {
      if ((packetBuffer[*snmpCursor] & 0x80) == 0) {
        OID[i] = packetBuffer[*snmpCursor];
        (*snmpCursor)++;
      }
      else {
        byte j = *snmpCursor;
        while (packetBuffer[j++] & 0x80) { // we son't support > 2 bytes so we must let snmpCursor pointing to next byte in case the value takes 3+ bytes
          OIDLength--;
        }
        if (j - *snmpCursor > 2) {
          OID[i] = -1; // buffer overflow
        } else {
          byte val1 = packetBuffer[*snmpCursor] & 0x7f;
          (*snmpCursor)++;
          byte val2 = packetBuffer[*snmpCursor] & 0x7f;
          (*snmpCursor)++;
          OID[i] = val1 << 7 | val2;
        }
        *snmpCursor = j;
      }
    }
  }
  //return number of decimal components. Example: 1.3.200.200 -> 5 bytes, but we return 4 components
  return OIDLength;
}


/**
 * parseOctecString function: Parse OctetString not larger than limit and stores it into value[]
 **/
byte parseOctecString (char value[], int *snmpCursor, int limit) {
  int valueLength = parseVLQ(&(*snmpCursor));
  //(*snmpCursor)++; //  same as *snmpCursor = *snmpCursor + 1;
  for (int i = 0; i < valueLength && i < limit; i++) {
    value[i] = (char)packetBuffer[*snmpCursor];
    (*snmpCursor)++;
  }
  if (valueLength < limit) {
    value[valueLength] = '\0';
  }
  else {
    value[limit] = '\0';
  }
  return 1;
}

/**
 * parseInt function: Parses large and small INT values which are divided in bytes.
 **/
unsigned long parseInt(int *snmpCursor, int valueLength) {
  unsigned long value;
  if (valueLength > 1) { // long integer
    unsigned long val1;
    unsigned long val2;
    byte iterations = 1;
    val1 = packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    val2 = packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    value = val1 << (8 * (valueLength - 1)) | val2 << (8 * (valueLength - 2));
    for (byte i = 0; i < valueLength - 2; i++) {
      val1 = packetBuffer[*snmpCursor];
      value = value | val1 << (8 * (valueLength - iterations - 2));
      (*snmpCursor)++;
      iterations++;
    }
  }
  else if (valueLength == 1) {
    value = (int) packetBuffer[*snmpCursor];
    (*snmpCursor)++; // let snmpCursor pointing to next byte
  }

  return value;
}

/**
 * readButton function: Read button states.
 **/
int readButton()
{
  adc_key_in = analogRead(0);      // leer el valor del sensor
  // ranges: 0, 144, 329, 504, 741
  if (adc_key_in > 1000) return btnNONE; // Most common response
  // For V1.1 us this threshold
  /*
  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 250)  return btnUP;
  if (adc_key_in < 450)  return btnDOWN;
  if (adc_key_in < 650)  return btnLEFT;
  if (adc_key_in < 850)  return btnSELECT;
  */

  // Para V1.0 utilizar estos umbrales:

  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 195)  return btnUP;
  if (adc_key_in < 380)  return btnDOWN;
  if (adc_key_in < 555)  return btnLEFT;
  if (adc_key_in < 790)  return btnSELECT;

  return btnNONE;  // no button is pressed. This statement should'nt be reached
}

void reverseArray(int first, int last) {
  byte buff;
  while (first < last) {
    buff = packetBuffer[first];
    packetBuffer[first] = packetBuffer[last];
    packetBuffer[last] = buff;
    first++;
    last--;
  }
}

