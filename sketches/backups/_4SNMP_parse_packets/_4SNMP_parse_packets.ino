#include <MemoryFree.h>

#include <SPI.h>         //  needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         //  UDP library from: bjoern@cs.stanford.edu 12/30/2008
// #include <Streaming.h>         // Alternativa para convertir bytes en hex

#include "MyTypes.h"

IPAddress local(169, 254, 123, 123);
IPAddress remote(169, 254, 123, 122);
UDP_struct udp = {
  {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}
  , local, remote, 8888};

//  buffer to receive data
byte packetBuffer[UDP_TX_PACKET_MAX_SIZE]; 

//  UDP instance
EthernetUDP Udp;

void setup() {
  Serial.begin(9600);
  Serial.print("Free RAM: ");
  Serial.println(freeMemory());
  // Start Ethernet and UDP
  Ethernet.begin(udp.mac,udp.local_IP);
  Udp.begin(udp.localPort);

  // He leido que es recomendable activar el pin 4 al utilizar UDP, no sé muy bien por qué. Investigar.
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);
}

void loop() {
  // test packet
//  byte test[UDP_TX_PACKET_MAX_SIZE] = {
//    0x30, 0x26, 0x02, 0x01, 0x0, 0x04, 0x06, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0xa0, 0x19, 0x02, 0x01, 0x26, 
//    0x02, 0x01, 0x0, 0x02, 0x01, 0x00, 0x30, 0x0e, 0x30, 0x0c, 0x06, 0x08, 0x2b, 0x06, 0x01, 0x02, 0x01, 0x01, 0x02, 0x0, 0x05, 0x0  };

  // sysUpTime. Comunidad = holaholita
  byte sysup[UDP_TX_PACKET_MAX_SIZE] = {
    0x30,0x2a,0x02,0x01,0x0,0x04,0x0a,0x68,0x6f,0x6c,0x61,0x68,0x6f,0x6c,0x69,0x74,0x61,0xa0,0x19,0x02,0x01,0x05,0x02,0x01,0x0,
    0x02,0x01,0x0,0x30,0x0e,0x30,0x0c,0x06,0x08,0x2b,0x06,0x01,0x02,0x01,0x01,0x03,0x0,0x05,0x0  };

  //  sysName. Comunidad = holaholita
//  byte sysup[UDP_TX_PACKET_MAX_SIZE] = {
//    0x30,0x2a,0x02,0x01,0x00,0x04,0x0a,0x68,0x6f,0x6c,0x61,0x68,0x6f,0x6c,0x69,0x74,0x61,0xa0,0x19,0x02,0x01,0x0b,0x02,0x01,0x00,
//    0x02,0x01,0x00,0x30,0x0e,0x30,0x0c,0x06,0x08,0x2b,0x06,0x01,0x02,0x01,0x01,0x05,0x00,0x05,0x00    };

  Udp.beginPacket(udp.remote_IP, 161);
  // Build and send message. BEWARE!! We are writing trash here if we use sizeof(sysup). It's better to use packet size, which is:
  // sysup[1] + 2 (SNMPSequenceLength + 2 firsts bytes (message type and SNMPSequenceLength itself))
  //Serial.println("bytes a escribir: ");
  //Serial.print(sizeof(sysup));
  for(int i = 0; i < (int) sysup[1] + 2; i++){
    Udp.write(sysup[i]); 
  }
  Serial.println();
  if(DEBUG)
    Serial.println("paquete enviado");
  Udp.endPacket();

  delay(2000);

  // Recibir respuesta
  int packetSize = Udp.parsePacket();
  if(Udp.available()){
    // limpiar buffer para evitar problemas. Válido para char. Testear con bytes!!!
    // for(int i = 0; i < UDP_TX_PACKET_MAX_SIZE; i++){
    //   packetBuffer[i] = (char)0;
    // }

    if(packetSize < UDP_TX_PACKET_MAX_SIZE){
      Serial.print("UDP packet received. Size (bytes): ");
      Serial.println(packetSize);
      Serial.print("From ");
      IPAddress remote = Udp.remoteIP();
      for (int i =0; i < 4; i++)
      {
        Serial.print(remote[i], DEC);
        if (i < 3)
        {
          Serial.print(".");
        }
      }
      Serial.print(", port ");
      Serial.println(Udp.remotePort());

      //  read packetSize bytes and store them into packetBuffer
      Udp.read(packetBuffer, packetSize);


      SNMP_struct receivedPacket;
      parsePacket(packetSize, &receivedPacket);
      
      Serial.println("  Details: ");
      Serial.print("  Version: ");
      Serial.print(receivedPacket.snmpVersion);
      Serial.print(". Community: ");
      Serial.println(receivedPacket.community);
      Serial.print("  Request ID: ");
      Serial.print(receivedPacket.requestID);

      if(receivedPacket.pduType == 0xA0)
        Serial.println(". PDU type: GetRequest"); //Not Supported
      else if (receivedPacket.pduType == 0xA2)
        Serial.println(". PDU type: GetResponse");
      else if (receivedPacket.pduType == 0xA3)
        Serial.println(". PDU type: SetRequest"); //Not supported

      Serial.print("  OID: ");
      for (int i = 0; i < receivedPacket.OIDLength;i++){
        Serial.print(receivedPacket.OID[i], HEX);
        Serial.print(" ");
      }

      if(receivedPacket.valueType == 0x02 || receivedPacket.valueType == 0x41 || receivedPacket.valueType == 0x42 || receivedPacket.valueType == 0x43){
        Serial.print(". Value: ");
        Serial.println(receivedPacket.valueInt);
      }
      else if (receivedPacket.valueType == 0x04){
        Serial.print(". Value: ");
        Serial.println(receivedPacket.valueChar);
      }
      Serial.print("Free RAM: ");
      Serial.println(freeMemory());
      delay(10000);

      if(DEBUG){
        Serial.println("Content:");
        for(int x = 0; x < packetSize; x++) {
          if(x%16 == 0 && x != 0) 
            Serial.println();
          Serial.print(packetBuffer[x], HEX);   
          Serial.print(", ");
        }
        Serial.print("\n\n");
      }
    }
    else{
      Serial.println("Packet too large");
    }
    Serial.println();
  }
}

int parsePacket(int packetSize, SNMP_struct *receivedPacket){

  int snmpCursor = 0;
  // check we received an SNMP message
  if(packetBuffer[snmpCursor] != 0x30){
    Serial.println("not an SNMP message");
    return 0;
  }
  else{
    Serial.println("  SNMP message received. Parsing packet...");
    receivedPacket->snmp_length = (int) packetBuffer[++snmpCursor];

    // parse SNMP version
    if(packetBuffer[++snmpCursor] == 0x02 && packetBuffer[++snmpCursor] == 0x01){ // correct version field
      if(packetBuffer[++snmpCursor] == 0x00)
        receivedPacket->snmpVersion = 1;
      else if(packetBuffer[snmpCursor] == 0x01)
        receivedPacket->snmpVersion = 2;
      else if(packetBuffer[snmpCursor] == 0x02)
        receivedPacket->snmpVersion = 3;
    }
    else{
      receivedPacket->snmpVersion = 0; //wrong version
      return 0;
    }

    // parse community string
    if(packetBuffer[++snmpCursor] == 0x04){ // correct community string
      snmpCursor++; // let snmpCursor pointing to community string length
      receivedPacket->community_length = (int) packetBuffer[snmpCursor];
      char * p = parseOctecString(&snmpCursor);
      strcpy(receivedPacket->community, p);
      free(p);
    }
    else{
      return 0; //Wrong community string
    }

    // parse PDU type
    receivedPacket->pduType = packetBuffer[snmpCursor];
    snmpCursor++;

    receivedPacket->pdu_length = (int) packetBuffer[snmpCursor];
    snmpCursor++;

    // parse request id
    if(packetBuffer[snmpCursor] == 0x02){ // correct request id type (integer)
      receivedPacket->requestID = parseInt(&snmpCursor);
    }

    // parse error
    if(packetBuffer[snmpCursor] == 0x02){ //  correct error type (integer)
      receivedPacket->error = (int) parseInt(&snmpCursor);
    }

    // parse errorIndex
    if(packetBuffer[snmpCursor] == 0x02){ //  correct error type (integer)
      receivedPacket->errorIndex = (int) parseInt(&snmpCursor);
    }

    // parse varBind
    if(packetBuffer[snmpCursor] == 0x30){ // correct varbind type (sequence)
      snmpCursor += 4; //let snmpCursor pointing to first OID byte. We ignore multiple OIDs at the moment.
      receivedPacket->OIDLength = parseOID(&snmpCursor, receivedPacket->OID);
    }

    // parse Integers, Counters, Gauges and Timeticks
    if(packetBuffer[snmpCursor] == 0x02 || packetBuffer[snmpCursor] == 0x41 || packetBuffer[snmpCursor] == 0x42 || packetBuffer[snmpCursor] == 0x43){
      receivedPacket->valueType = packetBuffer[snmpCursor];
      receivedPacket->valueInt = parseInt(&snmpCursor);
    }
    //parse octec strings
    else if(packetBuffer[snmpCursor] == 0x04){
      receivedPacket->valueType = packetBuffer[snmpCursor];
      snmpCursor++; //let snmpCursor pointing to value string length
      char * p = parseOctecString(&snmpCursor);
      strcpy(receivedPacket->valueChar, p);
      free(p);
    }

    if(snmpCursor >= packetSize){
      Serial.print("  Packet parsing ended successfully. snmpCursor: ");
      Serial.println(snmpCursor);
      Serial.println();
      return 1;
    }
    else{
      // continue parsing next OID. Not supported
    }
  }
}

int parseOID(int *snmpCursor, byte OID[]){
  int OIDLength;
  if(packetBuffer[*snmpCursor] == 0x06){ // correct OID type
    (*snmpCursor)++;
    OIDLength = (int) packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    for(int i = 0; i < OIDLength; i++){
      OID[i] = packetBuffer[*snmpCursor];
      (*snmpCursor)++;
    }
  }
  return OIDLength;
}

// parse OctecString
char * parseOctecString (int *snmpCursor){
  int valueLength = (int) packetBuffer[*snmpCursor];
  (*snmpCursor)++; //  same as *snmpCursor = *snmpCursor + 1;
  char * buffer = (char *) malloc (valueLength);
  for(int i = 0; i < valueLength; i++){
    buffer[i] = (char)packetBuffer[*snmpCursor];
    (*snmpCursor)++;
  }
  buffer[valueLength] = '\0'; // avoid malloc problems
  return buffer;
}

// parse large and small int values
unsigned long parseInt(int *snmpCursor){
  unsigned long value;
  int type = (int) packetBuffer[*snmpCursor];
  (*snmpCursor)++;
  int valueLength = (int) packetBuffer[*snmpCursor];
  (*snmpCursor)++;
  if(type = 2 && valueLength > 1){ // long integer
    unsigned long val1;
    unsigned long val2;
    int iterations = 1;
    val1 = packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    val2= packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    value = val1 << (8 * (valueLength - 1)) | val2 << (8 * (valueLength - 2));
    if(DEBUG){
      Serial.println();
      Serial.print("two first values combined: ");
      Serial.print(value, HEX);
      Serial.println();
    }
    for(int i = 0; i < valueLength - 2; i++){
      val1 = packetBuffer[*snmpCursor];
      if(DEBUG){
        Serial.print("value to be combined: ");
        Serial.println (val1, HEX);
      }
      value = value | val1 << (8 * (valueLength - iterations - 2));
      if(DEBUG){
        Serial.print("combined: ");
        Serial.println (value, HEX);
      }
      (*snmpCursor)++;
      iterations++;
    }
  }
  else if (valueLength == 1){
    value = (int) packetBuffer[*snmpCursor];
  }
  (*snmpCursor)++; // let snmpCursor pointing to next byte
  return value;
}


//  function to check free ram
//int freeRam () {
//  extern int __heap_start, *__brkval; 
//  int v; 
//  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
//}


// problema:paquetes no limpios, códigos raros como FFFFFFA2, siendo A2 correcto.
// probar a convertir manualmente los bytes a HEX
// Es un problema de codificacion. Ver enlace:
// http:// stackoverflow.com/questions/8060170/printing-hexadecimal-characters-in-c
// Arreglado, había que declarar packetBuffer como un buffer de bytes.

// código antiguo, quizá útil en futuro. Conservar:
// Serial.println(packetBuffer);
// Serial << _HEX(packetBuffer[x]) << endl;
// String thisString = String(packetBuffer[x], HEX);
// packetBuffer[x] = packetBuffer[x] & 0xFF;


//   unsigned long a = 0x01;
//   unsigned long b = 0x00;
//   unsigned long c = 0x20;
//   unsigned long d = 0xF1;
// value = (a<<24) | (b<<16)| (c<<8) | d;
//   value = (a<<24) | b;
//   value = value << 16 | c;
//   value = value << 8 | d;

