// incluimos la libreria LiquidCrystal y las demás necesaras

#include <Wire.h>

#include <FastIO.h>
#include <I2CIO.h>
#include <LCD.h>
#include <LiquidCrystal.h>
#include <LiquidCrystal_I2C.h>
#include <LiquidCrystal_SR.h>
#include <LiquidCrystal_SR2W.h>
#include <LiquidCrystal_SR3W.h>

/*
 Ejemplo de uso de display LCD 16x2.  Usando la librería LiquidCrystal
 library compatible con el driver de Hitachi HD44780 driver 
 */

// inicializamos la librería con los numeros pins del interfaz
// cada LCD puede llevar sus propios numeros
LiquidCrystal lcd(8, 13, 9, 4, 5, 6, 7);

// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;
int contador = 0;
#define retraso 200
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

// leer los botones
int leerBotones()
{
  adc_key_in = analogRead(0);      // leer el valor del sensor
  // los botones cuando se leen dan estos valores: 0, 144, 329, 504, 741
  // pueden haber oscilaciones, así que se aproximan los valores
  if (adc_key_in > 1000) return btnNONE; // Primera opción por razones de eficiencia
  // For V1.1 us this threshold
  if (adc_key_in < 50)   return btnRIGHT;  
  if (adc_key_in < 250)  return btnUP; 
  if (adc_key_in < 450)  return btnDOWN; 
  if (adc_key_in < 650)  return btnLEFT; 
  if (adc_key_in < 850)  return btnSELECT;  

  // Para V1.0 utilizar estos umbrales:
  /*
 if (adc_key_in < 50)   return btnRIGHT;  
   if (adc_key_in < 195)  return btnUP; 
   if (adc_key_in < 380)  return btnDOWN; 
   if (adc_key_in < 555)  return btnLEFT; 
   if (adc_key_in < 790)  return btnSELECT;   
   */
  return btnNONE;  // cuando no hay botón pulsado, se devuelve esto
}

void setup()
{
  lcd.begin(16, 2);              // se inicializa la libreria con un lcd de 16x2
  lcd.setCursor(0,0);
  lcd.clear();
  lcd.print("Pulsa los botones");
}

void loop()
{
  lcd.setCursor(9,1);            // mover el cursor a la segunda línea, 9 espacios a la derecha
  //lcd.print(millis()/1000);      // segundos desde encendido
  lcd.print(contador);

  lcd.setCursor(0,1);            // cursor en el inicio de la segunda línea
  lcd_key = leerBotones(); 

  switch (lcd_key)               // dependiendo del botón se hace una acción distinta.
  {
  case btnRIGHT:
    {
      lcd.print("RIGHT ");
      break;
    }
  case btnLEFT:
    {
      lcd.print("LEFT   ");
      lcd.setCursor(0, 1);
      lcd.print("    ");
      delay(200);
      lcd.setCursor(0,1);
      lcd.print("NONE");
      break;
    }
  case btnUP:
    {
      lcd.print("UP    ");
      lcd.setCursor(9, 1);
      lcd.print(++contador);
      delay(retraso);
      if(contador > 255){
        contador = 0;
        lcd.setCursor(9, 1);
        lcd.print("   ");
        lcd.setCursor(9, 1);
        lcd.print(contador);
      }
      lcd.setCursor(0, 1);
      
      break;
      
    }
  case btnDOWN:
    {
      lcd.print("DOWN  ");
      lcd.setCursor(9, 1);
      lcd.print(--contador);
      //condiciones para arreglar errores de display
      if(contador == 9){
        lcd.setCursor(10, 1);
        lcd.print(" ");
      }
      else if(contador == 99){
        lcd.setCursor(11, 1);
        lcd.print(" ");
      }
      else if(contador < 0){
        contador = 255;
        lcd.setCursor(9, 1);
        lcd.print(contador);
      }
      delay(retraso);
      lcd.setCursor(0,1);
      break;
    }
  case btnSELECT:
    {
      lcd.print("SELECT");
      break;
    }
  case btnNONE:
    {
      lcd.print("NONE  ");
      break;
    }
  }

}


