#ifndef MyTypes_h
#define MyTypes_h
// tamaño máximo (bytes) del paquete UDP 
// #define UDP_TX_PACKET_MAX_SIZE 64
// standard maximum SNMP UDP packet size = 484. 128 will be ok for my porpuses. Allows 32 bytes for community, 24 for oid and 32 for value from GetResponse
#define UDP_TX_PACKET_MAX_SIZE 256

#define DEBUG 0
#define MAX_COMMUNITY_SIZE 32
#define MAX_OID_SIZE 24
#define MAX_VALUE_SIZE 64

typedef struct UDP_struct {
  byte mac[6];
  IPAddress local_IP;
  IPAddress remote_IP;
  unsigned int localPort; //  local port to listen on
} UDP_struct;


typedef struct SNMP_struct {
  int snmp_length;
  int snmpVersion;
  char community[MAX_COMMUNITY_SIZE];
  int community_length;
  unsigned long requestID;
  int requestID_length;
  byte error;
  byte errorIndex;
  byte pduType;
  int pdu_length;
  byte OID[MAX_OID_SIZE];
  int OIDLength;
  byte valueType;
  unsigned long valueInt;
  char valueChar[MAX_VALUE_SIZE];
  int varBindList_length;
  int varBind_length;
}SNMP_struct;

//Se puede ahorrar RAM si consiguiera no tener que reservar valueInt y valueChar

#endif

