/* 
 This sketch enables/disables all interfaces in a remote device
 */
#include <MemoryFree.h>

#include <SPI.h>         //  needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h> //  UDP library from: bjoern@cs.stanford.edu 12/30/2008
// #include <Streaming.h>         // Alternativa para convertir bytes en hex

#include "MyTypes.h"

IPAddress local(169, 254, 123, 123);
IPAddress remote(169, 254, 123, 122);
UDP_struct udp = {
  {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  }
  , local, remote, 8888};

//  buffer to send and receive data
byte packetBuffer[UDP_TX_PACKET_MAX_SIZE]; 
unsigned long requestID = 32760;

byte initialOID[MAX_OID_SIZE]  = {
  1,3,6,1,2,1,2,2,1,7,1
};
byte OID[MAX_OID_SIZE] = {
  1,3,6,1,2,1,2,2,1,7,1
};

int oidSize = 11;

//  UDP instance
EthernetUDP Udp;

void setup() {
  Serial.begin(9600);
  //Disable the SD card
  pinMode (4, OUTPUT);                  //disable SD card slot on ethernet shield, due to SPI bus
  digitalWrite (4, HIGH);  
  
  Serial.print("Free RAM: ");
  Serial.println(freeMemory());
  // Start Ethernet and UDP
  Ethernet.begin(udp.mac,udp.local_IP);
  Udp.begin(udp.localPort);
}

void loop() {
  // packet variables
  mode(1);
  Serial.print("Free RAM: ");
  Serial.println(freeMemory());
}

int mode(int mode){
  char community[] = "lool";
  unsigned long valueInt;
  if(mode == 1)
    valueInt = 1; // 1 -> up
  else if(mode == 2)
    valueInt = 2; // 2 -> down
    
  char valueChar[] = "";
  byte valueType = 0x02; // 0x05 -> null
  byte pduType = 0xA3; //GetRequest = 0xA0. SetRequest = 0xA3

  int generatedBytes = generatePacket(community, sizeof(community), OID, oidSize, valueType, valueInt, valueChar, sizeof(valueChar), pduType);
  requestID++;
  writePacket(generatedBytes);

  delay(1000);
  memset(&packetBuffer[0], 0, sizeof(packetBuffer));

  //... receive packet    
  int packetSize = Udp.parsePacket();
  if(Udp.available()){
    if(packetSize < UDP_TX_PACKET_MAX_SIZE){
      receivePacket(packetSize);
      SNMP_struct receivedPacket;
      parsePacket(packetSize, &receivedPacket);
      printPacket(packetSize, &receivedPacket);
      if(receivedPacket.error != 0x00)
        for(int i = 0; i < sizeof(initialOID); i++){
          OID[i] = initialOID[i];
        }
      else if(mode == 1 || mode == 2){
        OID[oidSize - 1]++;          
      }
      else if(mode == 3){
        
      }
    }
    else{
      Serial.println("Packet too large");
    }
    delay(1000);
    Serial.println();
  }
}

int receivePacket(int packetSize){
  Serial.print("UDP packet received. Size (bytes): ");
  Serial.println(packetSize);
  Serial.print("From ");
  IPAddress remote = Udp.remoteIP();
  for (int i =0; i < 4; i++)
  {
    Serial.print(remote[i], DEC);
    if (i < 3)
    {
      Serial.print(".");
    }
  }
  Serial.print(", port ");
  Serial.println(Udp.remotePort());

  //  read packetSize bytes and store them into packetBuffer
  Udp.read(packetBuffer, packetSize);
}

int writePacket(int generatedBytes){
  Udp.beginPacket(udp.remote_IP, 161);
  for(int i = generatedBytes; i >= 0; i--){
    Udp.write(packetBuffer[i]); 
  }
  Udp.endPacket();
  return 1;
}

void printPacket (int packetSize, SNMP_struct *receivedPacket){
  Serial.print("  Details: ");
  Serial.print("size: ");
  Serial.println(receivedPacket->snmp_length);
  Serial.print("  Version: ");
  Serial.print(receivedPacket->snmpVersion);
  Serial.print(". Community: ");
  Serial.println(receivedPacket->community);
  Serial.print("  Request ID: ");
  Serial.print(receivedPacket->requestID);

  if(receivedPacket->error == 0x02){
    Serial.print(". Error: No Such Name");
  }

  if(receivedPacket->pduType == 0xA0)
    Serial.println(". PDU type: GetRequest"); //Not Supported
  else if (receivedPacket->pduType == 0xA2)
    Serial.println(". PDU type: GetResponse");
  else if (receivedPacket->pduType == 0xA3)
    Serial.println(". PDU type: SetRequest"); //Not supported

  Serial.print("  OID: ");
  for (int i = 0; i < receivedPacket->OIDLength;i++){
    Serial.print(receivedPacket->OID[i], HEX);
    Serial.print(" ");
  }

  if(receivedPacket->valueType == 0x02 || receivedPacket->valueType == 0x41 || receivedPacket->valueType == 0x42 || receivedPacket->valueType == 0x43){
    Serial.print(". Value: ");
    Serial.println(receivedPacket->valueInt);
  }
  else if (receivedPacket->valueType == 0x04){
    Serial.print(". Value: ");
    Serial.println(receivedPacket->valueChar);
  }

  // debugging
  if(DEBUG){
    Serial.println("Content:");
    for(int x = 0; x < packetSize; x++) {
      if(x%16 == 0 && x != 0) 
        Serial.println();
      Serial.print(packetBuffer[x], HEX);   
      Serial.print(", ");
    }
    Serial.print("\n\n");
  }
}

/**
 * writes plain data into packetbuffer so it can be sent
 **/
int generatePacket(char community[], int communitySize, byte OID[], int OIDSize, byte valueType, unsigned long valueInt, char valueChar[], int valueCharSize, byte pduType){
  int  snmpCursor = 0;
  //write value. It can be null, Integer or OctetString
  int valueLength = countBytes(valueInt);
  if(valueType == 0x05){
    packetBuffer[snmpCursor++] = 0x00;
    packetBuffer[snmpCursor++] = 0x05;
  }
  else if(valueType == 0x02 && countBytes(valueInt) == 1){ // one byte integer
    packetBuffer[snmpCursor++] = valueInt;
    packetBuffer[snmpCursor++] = 0x01;
    packetBuffer[snmpCursor++] = 0x02;
  }
  else if (valueType == 0x02 && countBytes(valueInt) > 1){ // 2 or more bytes integer
    byte buffer[countBytes(valueInt)];
    writeLongValue(&buffer[0], valueInt);
    for(int i = countBytes(valueInt) - 1; i >= 0 ; i--){
      packetBuffer[snmpCursor++] = buffer[i];
    }
    packetBuffer[snmpCursor++] = countBytes(valueInt);
  }
  else if (valueType == 0x04){ // OctecString
    for(int i = valueCharSize - 2; i >= 0; i--){
      packetBuffer[snmpCursor++] = (byte) valueChar[i];
    }
    packetBuffer[snmpCursor++] = valueCharSize - 1;
    packetBuffer[snmpCursor++] = 0x04;
  }

  // write OID
  int realOIDSize = 0;
  for(int i = OIDSize - 1; i > 1; i--){
    if(countBytes(OID[i]) == 1){
      packetBuffer[snmpCursor] = OID[i];
      snmpCursor++;
      realOIDSize++;
    }
    else{
      byte buffer[countBytes(OID[i])];
      writeLongValue(&buffer[0], OID[i]);
      for(int i = countBytes(OID[i]) - 1; i >= 0 ; i--){
        packetBuffer[snmpCursor++] = buffer[i];
        realOIDSize++;
      }
    }
  }
  packetBuffer[snmpCursor++] = 0x2B; //0x2B == 1.3
  packetBuffer[snmpCursor++] = OIDSize - 1; //BEWARE! We are assumming each value from OID can be coded as 1 byte. 1.3 counts as 1 byte, so we must deduct 1.
  packetBuffer[snmpCursor++] = 0x06; //OID Type == 0x06
  packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
  snmpCursor++;
  packetBuffer[snmpCursor++] = 0x30;
  packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
  snmpCursor++;
  packetBuffer[snmpCursor++] = 0x30;

  // write (no) error codes
  for(int i = 0; i < 2; i++){ 
    for(int j = 0; j < 3; j++){
      packetBuffer[snmpCursor++] = j;
    }
  }

  //write request id
  if(countBytes(requestID) == 1){
    packetBuffer[snmpCursor++] = requestID; // Global variable, assume 1 byte
  }
  else{
    byte buffer[countBytes(requestID)];
    writeLongValue(&buffer[0], requestID);
    for(int i = countBytes(requestID) - 1; i >= 0 ; i--){
      packetBuffer[snmpCursor++] = buffer[i];
    }
  }
  packetBuffer[snmpCursor++] = countBytes(requestID);
  packetBuffer[snmpCursor++] = 0x02; // Integer type for requestID

  // write pdu type and size
  packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
  snmpCursor++;
  packetBuffer[snmpCursor++] = pduType;

  // write community
  for(int i = communitySize - 2; i >= 0; i--){
    packetBuffer[snmpCursor++] = (byte) community[i];
  }
  packetBuffer[snmpCursor++] = communitySize - 1;
  packetBuffer[snmpCursor++] = 0x04; //OctetString type for community
  for(int j = 0; j < 3; j++){ //Write community. 0 -> v1
    packetBuffer[snmpCursor++] = j;
  }

  //write packet size
  packetBuffer[snmpCursor] = snmpCursor; //AVOID packetBuffer[snmpCursor++] here
  snmpCursor++;
  packetBuffer[snmpCursor] = 0x30;

  return snmpCursor;
}

/**
 * Divides an unsigned long into bytes and writes it into *buffer
 **/
void writeLongValue(byte *buffer, unsigned long n){
  int bytes = countBytes(n);
  int iterations = 0;
  for(int i = bytes - 1; i >= 0; i--){
    buffer[i] = n >> 8 * iterations;
    iterations++;
  }
  //now in buffer we have n divided in bytes, ready to be sent
}

/**
 * Count bytes of an unsigned long in two compliment's form
 **/
int countBytes(unsigned long n){
  int bytes;  
  // get number size (bytes). As per ASN.1, most significant bit is not used for the value itself, so we have 7 bits for each number (128 numbers with 1 byte)
  if(n < 128){
    bytes = 1;
  }
  else if (n < 32768){ // We have 7 + 8 bits = 15 bits. 2^15 = 32768. Two compliment's form.
    bytes = 2;
  }
  else if (n < 8388608){
    bytes = 3;
  }
  else {
    bytes = 4;
  }
  return bytes;
}

/**
 * Not used at the moment
 **/
int countBytesLengthValues(unsigned long n){
  int bytes;  
  //get number size (bytes). As per ASN.1, we can use 8 bits for a number
  if(n < 256){
    bytes = 1;
  }
  else if (n < 65536){
    bytes = 2;
  }
  else if (n < 16777216){
    bytes = 3;
  }
  else {
    bytes = 4;
  }
  return bytes;
}

// parse variable length number
int parseVLQ(int *snmpCursor){
  int value;
  if(packetBuffer[*snmpCursor] & 0x80){ // if first bit is 1, then, the number is composed of 2 or more bytes. We asume we won't receive messages with length > 2 bytes (16384 bits)
    int bytes = packetBuffer[*snmpCursor] & 0x7F; // set to 0 most significant bit and get the number of bytes
    (*snmpCursor)++;
    value = parseInt(&(*snmpCursor), bytes);
  }
  else{
    value = packetBuffer[*snmpCursor];
    (*snmpCursor)++;
  }
  return value;
}

/**
 * This function reads a packet, saves variables in a struct and returns number of bytes read
 **/
int parsePacket(int packetSize, SNMP_struct *receivedPacket){

  int snmpCursor = 0;
  // check we received an SNMP message
  if(packetBuffer[snmpCursor] != 0x30){
    Serial.println("not an SNMP message");
    return 0;
  }
  else{
    Serial.println("  SNMP message received. Parsing packet...");
    snmpCursor++;
    receivedPacket->snmp_length = parseVLQ(&snmpCursor);

    // parse SNMP version
    if(packetBuffer[snmpCursor] == 0x02 && packetBuffer[++snmpCursor] == 0x01){ // correct version field
      if(packetBuffer[++snmpCursor] == 0x00)
        receivedPacket->snmpVersion = 1;
      else if(packetBuffer[snmpCursor] == 0x01)
        receivedPacket->snmpVersion = 2;
      else if(packetBuffer[snmpCursor] == 0x02)
        receivedPacket->snmpVersion = 3;
    }
    else{
      receivedPacket->snmpVersion = 0; //wrong version
      Serial.println("Wrong version");
      return 0;
    }

    // parse community string
    if(packetBuffer[++snmpCursor] == 0x04){ // correct community string
      snmpCursor++; // let snmpCursor pointing to community string length
      receivedPacket->community_length = (int) packetBuffer[snmpCursor];
      parseOctecString(receivedPacket->community, &snmpCursor, MAX_COMMUNITY_SIZE);
    }
    else{
      return 0; //Wrong community string
    }

    // parse PDU type
    receivedPacket->pduType = packetBuffer[snmpCursor];
    snmpCursor++;

    receivedPacket->pdu_length = parseVLQ(&snmpCursor);

    // parse request id
    if(packetBuffer[snmpCursor] == 0x02){ // correct request id type (integer)
      int valueLength = packetBuffer[++snmpCursor];
      snmpCursor++;
      receivedPacket->requestID = parseInt(&snmpCursor, valueLength);
    }

    // parse error
    if(packetBuffer[snmpCursor] == 0x02){ //  correct error type (integer)
      snmpCursor += 2;
      receivedPacket->error = packetBuffer[snmpCursor++];
    }

    // parse errorIndex
    if(packetBuffer[snmpCursor] == 0x02){ //  correct error type (integer)
      snmpCursor += 2;
      receivedPacket->errorIndex = packetBuffer[snmpCursor++];
    }

    // parse varBind
    if(packetBuffer[snmpCursor++] == 0x30){ // correct varbind type (sequence)
      //snmpCursor += 4; //let snmpCursor pointing to first OID byte. We ignore multiple OIDs at the moment.
      receivedPacket->varBindList_length = parseVLQ(&snmpCursor);
      if(packetBuffer[snmpCursor++] == 0x30){
        receivedPacket->varBind_length = parseVLQ(&snmpCursor);
        receivedPacket->OIDLength = parseOID(&snmpCursor, receivedPacket->OID);
      }
    }

    // parse Integers, Counters, Gauges and Timeticks
    if(packetBuffer[snmpCursor] == 0x02 || packetBuffer[snmpCursor] == 0x41 || packetBuffer[snmpCursor] == 0x42 || packetBuffer[snmpCursor] == 0x43){
      receivedPacket->valueType = packetBuffer[snmpCursor++];
      int valueLength = packetBuffer[snmpCursor++];
      receivedPacket->valueInt = parseInt(&snmpCursor, valueLength);
    }

    //parse octec strings
    else if(packetBuffer[snmpCursor] == 0x04){
      receivedPacket->valueType = packetBuffer[snmpCursor];
      snmpCursor++; //let snmpCursor pointing to value string length
      parseOctecString(receivedPacket->valueChar, &snmpCursor, MAX_VALUE_SIZE);
    }

    if(snmpCursor >= packetSize){
      Serial.print("  Packet parsing ended successfully. snmpCursor: ");
      Serial.println(snmpCursor);
      Serial.println();
      return 1;
    }
    else{
      // continue parsing next OID. Not supported
    }
  }
}

/**
 * Parse OID and save it into OID[]. Returns OID length
 **/
int parseOID(int *snmpCursor, byte OID[]){
  int OIDLength;
  if(packetBuffer[*snmpCursor] == 0x06){ // correct OID type
    (*snmpCursor)++;
    OIDLength = (int) packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    for(int i = 0; i < OIDLength; i++){
      OID[i] = packetBuffer[*snmpCursor];
      (*snmpCursor)++;
    }
  }
  return OIDLength;
}


/**
 * parse OctetString
 **/
int parseOctecString (char value[], int *snmpCursor, int limit){
  int valueLength = parseVLQ(&(*snmpCursor));
  //(*snmpCursor)++; //  same as *snmpCursor = *snmpCursor + 1;
  for(int i = 0; i < valueLength && i < limit; i++){
    value[i] = (char)packetBuffer[*snmpCursor];
    (*snmpCursor)++;
  }
  if(valueLength < limit){
    value[valueLength] = '\0';
  } 
  else{
    value[limit] = '\0';
  }
  return 1;
}

/**
 * parse large and small int values which are divided in bytes.
 **/
unsigned long parseInt(int *snmpCursor, int valueLength){
  unsigned long value;
  if(valueLength > 1){ // long integer
    unsigned long val1;
    unsigned long val2;
    int iterations = 1;
    val1 = packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    val2 = packetBuffer[*snmpCursor];
    (*snmpCursor)++;
    value = val1 << (8 * (valueLength - 1)) | val2 << (8 * (valueLength - 2));
    if(DEBUG){
      Serial.println();
      Serial.print("two first values combined: ");
      Serial.print(value, HEX);
      Serial.println();
    }
    for(int i = 0; i < valueLength - 2; i++){
      val1 = packetBuffer[*snmpCursor];
      if(DEBUG){
        Serial.print("value to be combined: ");
        Serial.println (val1, HEX);
      }
      value = value | val1 << (8 * (valueLength - iterations - 2));
      if(DEBUG){
        Serial.print("combined: ");
        Serial.println (value, HEX);
      }
      (*snmpCursor)++;
      iterations++;
    }
  }
  else if (valueLength == 1){
    value = (int) packetBuffer[*snmpCursor];
    (*snmpCursor)++; // let snmpCursor pointing to next byte
  }

  return value;
}


//not used atm
int writeLongLengthValue(byte *buffer, int n){
  int bytes = countBytesLengthValues(n);
  buffer[bytes - 1] = bytes | 0x80;
  int iterations = 0;
  for(int i = bytes - 1; i >= 0; i--){
    buffer[i] = n >> 8 * iterations;
    iterations++;
  }
  //now in buffer we have n divided in bytes, ready to be sent
  return bytes + 1;
}

//Problema: longituded de OID variables. Formar estructura para arrays (array + size)?











