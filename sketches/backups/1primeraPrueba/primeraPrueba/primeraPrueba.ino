#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008

#define UDP_TX_PACKET_MAX_SIZE 64

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(169, 254, 123, 123);
IPAddress ipremota(169, 254, 123, 122);

unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //buffer to hold incoming packet
char  ReplyBuffer[] = "acknowledged";       // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;


void setup() {
  // start the Ethernet and UDP:
  Ethernet.begin(mac,ip);
  Udp.begin(localPort);
  
  //He leido que es recomendable hacer esto para no tener problemas con UDP. Just in case...
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);

  Serial.begin(9600);
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if(Udp.available())
  {
    //limpiar buffer para evitar problemas:
    for(int i = 0; i < UDP_TX_PACKET_MAX_SIZE; i++){
      packetBuffer[i] = (char)0;
    }
    
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i =0; i < 4; i++)
    {
      Serial.print(remote[i], DEC);
      if (i < 3)
      {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(packetBuffer);

    // send a reply, to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write("Has escrito: ");
    Udp.write(packetBuffer);
    Serial.println("Paquete recibido. Respondiendo...");
    Serial.println("--------------fin--------------");
    Udp.endPacket();
  }
  delay(10);
}

//Enviar paquete udp con NETCAT:
//nc 169.254.123.123 8888 -u (-u indica que se utiliza UDP)

//Código antiguo, podría ser útil. Es mejor no utilizar la librería String, ocupa demasiado (sobre 2k).
//    String respuesta = "Has dicho: ";
//    respuesta+=packetBuffer;
//    char bufferRespuesta [UDP_TX_PACKET_MAX_SIZE];
//    respuesta.toCharArray(bufferRespuesta, UDP_TX_PACKET_MAX_SIZE);
//    Serial.println("Paquete recibido, respondiendo...");
//    Serial.println(bufferRespuesta);
//    Udp.write(bufferRespuesta);
