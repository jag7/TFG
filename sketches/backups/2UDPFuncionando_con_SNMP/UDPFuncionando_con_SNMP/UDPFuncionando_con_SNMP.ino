#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
//#include <Streaming.h>         //Alternativa para convertir bytes en hex

//tamaño máximo (bytes) del paquete UDP
#define UDP_TX_PACKET_MAX_SIZE 64

byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(169, 254, 123, 123);
IPAddress ipremota(169, 254, 123, 122);

unsigned int localPort = 8888;      // local port to listen on

// buffer para recibir datos
byte packetBuffer[UDP_TX_PACKET_MAX_SIZE]; 

// Instancia UDP para enviar y recibir paquetes
EthernetUDP Udp;


void setup() {
  //Se inicia Ethernet y UDP
  Ethernet.begin(mac,ip);
  Udp.begin(localPort);
  
  //He leido que es recomendable activar el pin 4 al utilizar UDP, no sé muy bien por qué. Investigar.
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);

  Serial.begin(9600);
}

void loop() {
    //paquete de prueba
    byte test[] = {0x30, 0x26, 0x02, 0x01, 0x0, 0x04, 0x06, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0xa0, 0x19, 0x02, 0x01, 0x26, 
      0x02, 0x01, 0x0, 0x02, 0x01, 0x00, 0x30, 0x0e, 0x30, 0x0c, 0x06, 0x08, 0x2b, 0x06, 0x01, 0x02, 0x01, 0x01, 0x02, 0x0, 0x05, 0x0};
    //paquete para consultar sysUpTime
    byte sysup[] = {0x30,0x2a,0x02,0x01,0x0,0x04,0x0a,0x68,0x6f,0x6c,0x61,0x68,0x6f,0x6c,0x69,0x74,0x61,0xa0,0x19,0x02,0x01,0x05,0x02,0x01,0x0,
      0x02,0x01,0x0,0x30,0x0e,0x30,0x0c,0x06,0x08,0x2b,0x06,0x01,0x02,0x01,0x01,0x03,0x0,0x05,0x0};
    
    Udp.beginPacket(ipremota, 161);
    //Se construye y envia el paquete
    for(int i = 0; i < sizeof(sysup); i++){
      Udp.write(sysup[i]);
    }

    Udp.endPacket();
    delay(5000);
    
    //Recibir respuesta
    int packetSize = Udp.parsePacket();
    if(packetSize){
      //limpiar buffer para evitar problemas. Válido para char. Testear con bytes!!!
      //for(int i = 0; i < UDP_TX_PACKET_MAX_SIZE; i++){
      //  packetBuffer[i] = (char)0;
      //}
      
      Serial.print("Paquete recibido. Tamaño (bytes): ");
      Serial.println(packetSize);
      Serial.print("De ");
      IPAddress remote = Udp.remoteIP();
      for (int i =0; i < 4; i++)
      {
        Serial.print(remote[i], DEC);
        if (i < 3)
        {
          Serial.print(".");
        }
      }
      Serial.print(", puerto ");
      Serial.println(Udp.remotePort());

      // leer contenido del paquete y almacenarlo en el buffer
      Udp.read(packetBuffer,packetSize);
    
      Serial.println("Contenido:");
        for(int x = 0; x < packetSize; x++) {
          if(x%16 == 0 && x != 0) 
            Serial.println();
            Serial.print(packetBuffer[x], HEX);   
            Serial.print(", ");
        }
      Serial.print("\n\n");
    }
}

    //problema:paquetes no limpios, códigos raros como FFFFFFA2, siendo A2 correcto.
    //probar a convertir manualmente los bytes a HEX
    //Es un problema de codificacion. Ver enlace:
    //http://stackoverflow.com/questions/8060170/printing-hexadecimal-characters-in-c
    //Arreglado, había que declarar packetBuffer como un buffer de bytes.
    
    //código antiguo, quizá útil en futuro. Conservar:
    //Serial.println(packetBuffer);
    //Serial << _HEX(packetBuffer[x]) << endl;
    //String thisString = String(packetBuffer[x], HEX);
    //packetBuffer[x] = packetBuffer[x] & 0xFF;

