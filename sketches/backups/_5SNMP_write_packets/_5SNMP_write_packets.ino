#include <MemoryFree.h>

#include <SPI.h>         //  needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         //  UDP library from: bjoern@cs.stanford.edu 12/30/2008
// #include <Streaming.h>         // Alternativa para convertir bytes en hex

#include "MyTypes.h"

IPAddress local(169, 254, 123, 123);
IPAddress remote(169, 254, 123, 122);
UDP_struct udp = {
  {
    0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
  }
  , 
  local, remote, 8888};

//  buffer to receive data
byte packetBuffer[UDP_TX_PACKET_MAX_SIZE]; 
byte sysup[UDP_TX_PACKET_MAX_SIZE];
unsigned long requestID = 16380;

//  UDP instance
EthernetUDP Udp;

void setup() {
  Serial.begin(9600);
  Serial.print("Free RAM: ");
  Serial.println(freeMemory());
  // Start Ethernet and UDP
  Ethernet.begin(udp.mac,udp.local_IP);
  Udp.begin(udp.localPort);

  // He leido que es recomendable activar el pin 4 al utilizar UDP, no sé muy bien por qué. Investigar.
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);
}

void loop() {

  //write packet
  char community[] = "holaholita";
  byte OID[] = {
    1, 3, 6, 1, 2, 1, 1, 3, 0                  };
  unsigned long valueInt;
  char valueChar[MAX_VALUE_SIZE];
  byte valueType = 0x05; // 0x05 -> null
  byte pduType = 0xA0; //GetRequest

  int writtenBytes = writePacket(community, sizeof(community), OID, sizeof(OID), valueType, valueInt, valueChar, pduType);
  requestID++;

  Udp.beginPacket(udp.remote_IP, 161);
  for(int i = writtenBytes; i >= 0; i--){
    Udp.write(sysup[i]); 
  }

  Udp.endPacket();

  delay(2000);

  //... receive pack
  
  
  

}

int writePacket(char community[], int communitySize, byte OID[], int OIDSize, byte valueType, unsigned long valueInt, char valueChar[], byte pduType){
  int  snmpCursor = 0;
  int valueLength = countBytes(valueInt);
  if(valueType == 0x05){
    sysup[snmpCursor++] = 0x00;
    sysup[snmpCursor++] = 0x05;
  }
  else if(valueType == 0x02 && countBytes(valueInt) == 1){ // one byte integer
    sysup[snmpCursor++] = valueInt;
    sysup[snmpCursor++] = 0x01;
    sysup[snmpCursor++] = 0x02;
  }
  else if (valueType == 0x02 && countBytes(valueInt) > 1){ // 2 or more bytes integer
    byte buffer[countBytes(valueInt)];
    writeLongValue(&buffer[0], valueInt);
    for(int i = countBytes(valueInt) - 1; i >= 0 ; i--){
      sysup[snmpCursor++] = buffer[i];
    }
  }
  else if (valueType == 0x04){ // OctecString
    for(int i = sizeof(valueChar) - 2; i >= 0; i--){
      sysup[snmpCursor++] = (byte) valueChar[i];
    }
    sysup[snmpCursor++] = sizeof(valueChar) - 1;
    sysup[snmpCursor++] = 0x04;
  }
  int realOIDSize = 0;
  for(int i = OIDSize - 1; i > 1; i--){
    if(countBytes(OID[i]) == 1){
      sysup[snmpCursor] = OID[i];
      snmpCursor++;
      realOIDSize++;
    }
    else{
      byte buffer[countBytes(OID[i])];
      writeLongValue(&buffer[0], OID[i]);
      for(int i = countBytes(OID[i]) - 1; i >= 0 ; i--){
        sysup[snmpCursor++] = buffer[i];
        realOIDSize++;
      }
    }
  }
  sysup[snmpCursor++] = 0x2B; //0x2B == 1.3
  sysup[snmpCursor++] = OIDSize - 1; //BEWARE! We are assumming each value from OID can be coded as 1 byte. 1.3 counts as 1 byte, so we must deduct 1.
  sysup[snmpCursor++] = 0x06; //OID Type == 0x06
  sysup[snmpCursor++] = snmpCursor;
  sysup[snmpCursor++] = 0x30;
  sysup[snmpCursor++] = snmpCursor;
  sysup[snmpCursor++] = 0x30;
  for(int i = 0; i < 2; i++){ // write (no) error codes
    for(int j = 0; j < 3; j++){
      sysup[snmpCursor++] = j;
    }
  }
  if(countBytes(requestID) == 1){
    sysup[snmpCursor++] = requestID; // Global variable, assume 1 byte
  }
  else{
    byte buffer[countBytes(requestID)];
    writeLongValue(&buffer[0], requestID);
    for(int i = countBytes(requestID) - 1; i >= 0 ; i--){
      sysup[snmpCursor++] = buffer[i];
    }
  }
  sysup[snmpCursor++] = countBytes(requestID);
  sysup[snmpCursor++] = 02; // Integer type for requestID

  sysup[snmpCursor++] = snmpCursor;
  sysup[snmpCursor++] = pduType;

  for(int i = communitySize - 2; i >= 0; i--){
    sysup[snmpCursor++] = (byte) community[i];
  }
  sysup[snmpCursor++] = communitySize - 1;
  sysup[snmpCursor++] = 0x04; //OctetString type for community
  for(int j = 0; j < 3; j++){ //Write community. 0 -> v1
    sysup[snmpCursor++] = j;
  }
  sysup[snmpCursor++] = snmpCursor;
  sysup[snmpCursor] = 0x30;

  return snmpCursor;
}

void writeLongValue(byte *buffer, unsigned long n){
  int bytes = countBytes(n);
  int iterations = 0;
  for(int i = bytes - 1; i >= 0; i--){
    buffer[i] = n >> 8 * iterations;
    iterations++;
  }
  //now in buffer we have n divided in bytes, ready to be sent
}

int countBytes(unsigned long n){
  int bytes;  
  //get number size (bytes). As per ASN.1, most significant byte is not used for the value itself, so we have 7 bits for each number (128 numbers with 1 byte)
  if(n < 127){
    bytes = 1;
  }
  else if (n < 16384){
    bytes = 2;
  }
  else if (n < 2097152){
    bytes = 3;
  }
  else {
    bytes = 4;
  }
  return bytes;
}













