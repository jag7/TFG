#include <SPI.h>
#include <Ethernet.h>
#include <ICMPPing.h>

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte ip[] = {192, 168, 1, 51};
IPAddress pingAddr(192, 168, 1, 6);

SOCKET pingSocket = 0;

ICMPPing ping(pingSocket, (uint16_t)random(0, 255));

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Trying DHCP..."));
  // start Ethernet
  if (Ethernet.begin(mac) == 0) {
    Serial.println(F("Failed to configure Ethernet using DHCP"));
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip);
    Serial.print(F("Static ip: "));
    for (byte i = 0; i < 4; i++) {
      Serial.print(ip[i], DEC);
      Serial.print(F("."));
    }
  }
  else {
    Serial.print(F("DHCP IP: "));
    for (byte i = 0; i < 4; i++) {
      Serial.print(Ethernet.localIP()[i], DEC);
      Serial.print(F("."));
    }
    Serial.println();
  }
}

void loop()
{
  ICMPEchoReply echoReply = ping(pingAddr, 4);
  if (echoReply.status == SUCCESS)
  {
    Serial.print(F("Reply "));
    Serial.print(echoReply.data.seq);
    Serial.print(F(" from "));
    for (byte i = 0; i < 4; i++) {
      Serial.print(echoReply.addr[i]);
      Serial.print(F("."));
    }
    Serial.print(F(" bytes = "));
    Serial.print(REQ_DATASIZE);
    Serial.print(F(" time = "));
    Serial.print(millis() - echoReply.data.time);
    Serial.print(F(" ttl = "));
    Serial.println(echoReply.ttl);
  }
  else
  {
    Serial.print(F("Ping failed: "));
    Serial.println(echoReply.status);
  }
  delay(2000);

}
