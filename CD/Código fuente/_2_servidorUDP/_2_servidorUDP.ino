#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
//maximum SNMP UDP packet size
#define UDP_TX_PACKET_MAX_SIZE 128

byte mac[] = {  
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress local_IP(169, 254, 123, 123);

unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //buffer to hold incoming packet

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

void setup() {
  // start the Ethernet and UDP:
  Ethernet.begin(mac,local_IP);
  Udp.begin(localPort);

  pinMode (4, OUTPUT);                  //disable SD card slot on ethernet shield, due to SPI bus
  digitalWrite (4, HIGH);

  Serial.begin(9600);
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if(packetSize)
  {
    if(packetSize > UDP_TX_PACKET_MAX_SIZE - 1){ // - 1 ->New line
      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
      Udp.write("Packet too large\n");
      Serial.println("Packet too large");
      Serial.println("--------------end--------------");
      Udp.endPacket();
    }
    else{
      //clear buffer:
      for(int i = 0; i < UDP_TX_PACKET_MAX_SIZE; i++){
        packetBuffer[i] = (char)0;
      }

      Serial.print("Received packet of size ");
      Serial.println(packetSize);
      Serial.print("From ");
      IPAddress remote = Udp.remoteIP();
      for (int i =0; i < 4; i++)
      {
        Serial.print(remote[i], DEC);
        if (i < 3)
        {
          Serial.print(".");
        }
      }
      Serial.print(", port ");
      Serial.println(Udp.remotePort());

      // read the packet into packetBufffer
      Udp.read(packetBuffer, packetSize);
      Serial.println("Contents:");
      Serial.println(packetBuffer);

      // send a reply, to the IP address and port that sent us the packet we received
      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
      Udp.write("Has escrito: ");
      Udp.write(packetBuffer);
      Serial.println("Paquete recibido. Respondiendo...");
      Serial.println("--------------end--------------");
      Udp.endPacket();
    }
    delay(10);
  }
}

//Enviar paquete udp con NETCAT:
//cd C:\Users\Jeff\Dropbox\UCLM\4º\TFG\recursos\netcat-win32-1.12
//nc 169.254.123.123 8888 -u (-u indica que se utiliza UDP)
